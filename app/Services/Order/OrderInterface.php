<?php

namespace App\Services\Order;

use App\Services\Payment\PaymentSystemInterface;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Order;

interface OrderInterface
{
    /**
     * Create a new Order
     *
     * @param Customer $buyer
     * @return Order
     */
    public function create(Customer $buyer): Order;

    /**
     * Add an item to order
     *
     * @param Order $order
     * @param Item $item
     * @return Order
     */
    public function addItem(Order $order, Item $item): Order;

    /**
     * remove an item from order
     *
     * @param Order $order
     * @param Item $item
     * @return Order
     */
    public function removeItem(Order $order, Item $item): Order;

    /**
     * Get an order status
     *
     * @param Order $order
     * @return string
     */
    public function status(Order $order): string;

    /**
     * Pay process
     *
     * @param Order $order
     * @param PaymentSystemInterface $paymentSystem
     * @return boolean
     */
    public function pay(Order $order, PaymentSystemInterface $paymentSystem): bool;

    /**
     * Cancel an order
     *
     * @param Order $order
     * @return void
     */
    public function cancel(Order $order): void;
}