<?php

namespace App\Services\Payment;

interface PaymentSystemInterface
{
    /**
     * Make a payment
     *
     * @param float $amount
     * @param array $params
     * @return array
     */
    public function pay(float $amount, array $params): array;

    /**
     * Get a payment status info
     *
     * @return string
     */
    public function status(): string;
}